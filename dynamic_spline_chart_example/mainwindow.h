﻿#ifndef DYNAMIC_SPLINE_CHART_EXAMPLE_MAINWINDOW_H
#define DYNAMIC_SPLINE_CHART_EXAMPLE_MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QValueAxis>
#include <QtCore/QTimer>

QT_CHARTS_USE_NAMESPACE

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow() override;

public slots:
    void handleTimeout();
    void start();
    void stop();

private:
    Ui::MainWindow *ui;

    QSplineSeries *splineSeries;
    QChart *chart;
    QChartView *chartView;
    QValueAxis *axisX;
    QValueAxis *axisY;
    QTimer timer;
};


#endif
