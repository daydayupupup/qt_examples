﻿#include "main_window.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
    // 创建曲线的数据系列
    series = new QSplineSeries();
    // 添加数据点
    series->append(0, 6);
    series->append(2, 4);
    series->append(3, 8);
    series->append(7, 4);
    series->append(10, 5);

    // 创建图表对象
    splineChart = new QChart();
    // 设置图表名称
    splineChart->setTitle("Simple spline chart example");
    // 隐藏图例
    splineChart->legend()->hide();
    // 添加图表系列对象
    splineChart->addSeries(series);
    // 创建坐标轴
    splineChart->createDefaultAxes();
    // 设置 y 轴的范围从 0 到 10
    splineChart->axes(Qt::Vertical).first()->setRange(0, 10);

    // 创建图表视图对象
    chartView = new QChartView();
    // 启用抗锯齿渲染
    chartView->setRenderHint(QPainter::Antialiasing);
    // 添加图表对象
    chartView->setChart(splineChart);

    setCentralWidget(chartView);
}

MainWindow::~MainWindow() = default;
