﻿#ifndef SPLINE_CHART_EXAMPLE_MAIN_WINDOW_H
#define SPLINE_CHART_EXAMPLE_MAIN_WINDOW_H


#include <QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QSplineSeries>
#include <QtWidgets/QMainWindow>

QT_CHARTS_USE_NAMESPACE

class MainWindow : public QMainWindow {
Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow() override;

private:
    QChartView *chartView;
    QChart *splineChart;
    QSplineSeries *series;

};

#endif
