﻿#ifndef QT_ECHARTS_EXAMPLE_MAINWINDOW_H
#define QT_ECHARTS_EXAMPLE_MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow() override;

private slots:

    void on_btnStart_clicked();
    void on_btnStop_clicked();
    void updateChart();


private:
    Ui::MainWindow *ui;

    QTimer *timer;
};


#endif
