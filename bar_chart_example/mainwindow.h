﻿#ifndef DYNAMIC_SPLINE_CHART_EXAMPLE_MAINWINDOW_H
#define DYNAMIC_SPLINE_CHART_EXAMPLE_MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>

// 引入 Qt Charts 模块的命名空间
QT_CHARTS_USE_NAMESPACE

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow() override;

private:
    Ui::MainWindow *ui;

    QBarSeries *m_barSeries;
    QBarSet *m_barSet;
    QChart *m_chart;
};

#endif