﻿#include "mainwindow.h"
#include "ui_MainWindow.h"


MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);

    // 创建折线系列对象
    m_barSeries = new QBarSeries();

    m_barSet = new QBarSet("11");

    m_barSet->append(1);
    m_barSet->append(1);
    m_barSet->append(1);
    m_barSet->append(1);
    m_barSet->append(1);


    // 创建图表对象
    m_chart = new QChart();
    // 隐藏图例
    m_chart->legend()->hide();
    // 将折线系列添加到图表中
    m_chart->addSeries(m_barSeries);
    // 创建默认的轴
    m_chart->createDefaultAxes();

    // 将图表添加到图标视图中
    ui->chartView->setChart(m_chart);
    // 启动抗锯齿功能，是图表显示更加光滑和精细
    ui->chartView->setRenderHint(QPainter::Antialiasing);
}

MainWindow::~MainWindow() {
    delete ui;
}
